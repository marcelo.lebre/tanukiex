defmodule Tanukiex.CommitsTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock
  import Tanukiex.Commits
  alias Tanukiex.Commits.Action
  alias Tanukiex.Commits.Actions
  alias Tanukiex.Commits.Attributes
  alias Tanukiex.Commits.BuildStatus
  alias Tanukiex.Commits.Comment

  doctest Tanukiex.Commits

  @credentials Tanukiex.credentials("https://gitlab.com/api/v3/", "pUPk5Rjq4aY_gxFjDQtB")

  test "list commits by project" do
    use_cassette "commits#list_by_project" do
      response = @credentials |> list_by_project(1130437)
      assert response.status_code == 200
    end
  end

  test "create a commit with multiple files and actions" do
    use_cassette "commits#create" do
      actions = Action.create("a.rb", "b.rb", "test", "text") |> Actions.build
      commit = %Attributes{
                            branch_name: "master",
                            commit_message: "commit message test",
                            actions: actions,
                            author_email: "marcelo.lebre@gmail.com",
                            author_name: "Marcelo Lebre"
                          }
      response = @credentials |> create(1814909, commit)
      assert response.status_code == 201
      assert response.body.id == "1b2d456ee337b04df5e295214b413804e568dff9"
    end
  end

  test "get single commit" do
    use_cassette "commits#get_single_commit" do
      response = @credentials |> get_single_commit(1130437, "master")
      assert response.status_code == 200
      assert response.body.id == "5bd0d11688a12b09e8853f7f3bfa10bee476f23d"
    end
  end

  test "get diff on a commit" do
    use_cassette "commits#diff" do
      response = @credentials |> diff(1130437, "master")
      assert response.status_code == 200
      assert List.first(response.body).new_path == ".gitlab-ci.yml"
    end
  end

  test "get comments on a commit" do
    use_cassette "commits#comments" do
      response = @credentials |> comments(1130437, "master")
      assert response.status_code == 200
      assert List.first(response.body).note == "Nice picture man!"
    end
  end

  test "get statuses on a commit" do
    use_cassette "commits#statuses" do
      response = @credentials |> statuses(1130437, "master")
      assert response.status_code == 200
    end
  end

  test "add comment to commit" do
    use_cassette "commits#add_comment" do
      comment = %Comment{
                          note: "Nice picture man\!",
                          path: "dudeism.md",
                          line: 2,
                          line_type: "new"
                        }
      response = @credentials |> add_comment(1130437, "master", comment)
      assert response.status_code == 201
      assert response.body.note == "Nice picture man!"
    end
  end

  test "add build status to commit" do
    use_cassette "commits#build_status" do
      build_status = %BuildStatus{
                                   state: "success",
                                   ref: "master",
                                   name: "default",
                                   target_url: "https://docs.gitlab.com",
                                   description: "Short description"
                                 }

      response = @credentials |> add_build_status(1130437, "master", build_status)
      assert response.status_code == 201
    end
  end
end