defmodule Tanukiex.BranchTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock
  import Tanukiex.Branch
  require IEx

  doctest Tanukiex.Branch

  @credentials Tanukiex.credentials("https://gitlab.com/api/v3/", "ABC1234567")

  test "list branch by project" do
    use_cassette "branch#list_by_project" do
      response = @credentials |> list_by_project(1111)
      assert response.status_code == 200
      assert List.first(response.body).name == "master"
    end
  end

  test "get single repo branch" do
    use_cassette "branch#get_single_by_project" do
      response = @credentials |> get_single_by_project(1111, "master")
      assert response.status_code == 200
      assert response.body.name == "master"
    end
  end

  test "protect repo branch" do
    use_cassette "branch#protect" do
      response = @credentials |> protect(1111, "master")
      assert response.status_code == 200
    end
  end

  test "protect repo branch and letting devs push and merge" do
    use_cassette "branch#protect_with_push_and_merge" do
      response = @credentials |> protect(1111, "master", true, true)
      assert response.status_code == 200
    end
  end

  test "unprotect repo branch" do
    use_cassette "branch#unprotect" do
      response = @credentials |> unprotect(1111, "bugfix/upsert")
      assert response.status_code == 200
    end
  end

  test "create repo branch" do
    use_cassette "branch#create" do
      response = @credentials |> create(1111, "wubbalubba", "master")
      assert response.status_code == 201
    end
  end

  test "delete repo branch" do
    use_cassette "branch#delete" do
      response = @credentials |> delete(1111, "wubbalubba")
      assert response.status_code == 200
      assert response.body.branch_name == "wubbalubba"
    end
  end
end
