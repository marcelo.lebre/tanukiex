defmodule Tanukiex.BuildVariablesTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock
  import Tanukiex.BuildVariables
  require IEx

  doctest Tanukiex.BuildVariables

  @credentials Tanukiex.credentials("https://gitlab.com/api/v3/", "Kvpa6iBJUVAyCUQuzGCq")

  test "list build variables by project" do
    use_cassette "build_variables#list_by_project" do
      response = @credentials |> list_by_project(1130437)
      assert response.status_code == 200
      assert List.first(response.body).key == "TESTING_VARIABLE"
    end
  end

  test "get the details of a project's specific build variable" do
    use_cassette "build_variables#details" do
      response = @credentials |> details(1130437, "TESTING_VARIABLE")
      assert response.status_code == 200
      assert response.body.value == "WOOT"
    end
  end

  test "create a new build variable" do
    use_cassette "build_variables#create" do
      response = @credentials |> create(1130437, "TESTING_VARIABLE2", "WUBBA LUBBA DUB DUB")
      assert response.status_code == 201
    end
  end

  test "update a build variable" do
    use_cassette "build_variables#update" do
      response = @credentials |> update(1130437, "TESTING_VARIABLE2", "WUBBA LUBBA DUB DUB2")
      assert response.status_code == 200
      assert response.body.value == "WUBBA LUBBA DUB DUB2"
    end
  end

  test "remove a project's build variable" do
    use_cassette "build_variables#remove" do
      response = @credentials |> remove(1130437, "TESTING_VARIABLE2", "WUBBA LUBBA DUB DUB2")
      assert response.status_code == 200
    end
  end
end