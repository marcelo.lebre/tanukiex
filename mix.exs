defmodule Tanukiex.Mixfile do
  use Mix.Project

  def project do
    [app: :tanukiex,
     version: "0.1.0",
     elixir: "~> 1.3",
     name: "Tanukiex",
     description: "Elixir wrapper for GitLab API",
     package: package,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [applications: [:logger, :httpotion]]
  end

  defp deps do
    [{:poison, "~> 2.0"},
    {:httpotion, "~> 3.0.1"},
    {:ex_doc, "~> 0.12", only: :docs},
    {:exvcr, "~> 0.7", only: :test},
    {:inch_ex, "~> 0.5", only: :dev},
    {:dogma, "~> 0.1", only: :dev}]
  end

  defp package do
    [ maintainers: ["Marcelo Lebre"],
      licenses: ["MIT"],
      links: %{ "GitLab" => "https://gitlab.com/marcelo.lebre/tanukiex" } ]
  end
end
