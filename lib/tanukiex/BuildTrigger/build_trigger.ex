defmodule Tanukiex.BuildTrigger do
  import Tanukiex

  @moduledoc """
  This module maps all build triggers api resources to a matching function.

  *** API Reference: *** https://docs.gitlab.com/ce/api/build_triggers.html
  """

  @doc """
  Get a list of project's build triggers.  

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/build_triggers.html#list-project-triggers
  
    ```
    Tanukiex.BuildTrigger.list_by_project(credentials, 12345)
    ```
  """
  @spec list_by_project(Credentials.t, integer) :: Tanukiex.Response
  def list_by_project(credentials, project_id) do
    get(credentials, "/projects/#{project_id}/triggers")
  end

  @doc """
  Get details of project's build trigger.  

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/build_triggers.html#get-trigger-details
  
    ```
    Tanukiex.BuildTrigger.details(credentials, 12345, token)
    ```
  """
  @spec details(Credentials.t, integer, String.t) :: Tanukiex.Response
  def details(credentials, project_id, token) do
    get(credentials, "/projects/#{project_id}/triggers/#{token}")
  end

  @doc """
  Create a build trigger for a project.  

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/build_triggers.html#create-a-project-trigger
  
    ```
    Tanukiex.BuildTrigger.create(credentials, 12345)
    ```
  """
  @spec create(Credentials.t, integer) :: Tanukiex.Response
  def create(credentials, project_id) do
    post(credentials, "/projects/#{project_id}/triggers")
  end

  @doc """
  Remove a project's build trigger.

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/build_triggers.html#remove-a-project-trigger
  
    ```
    Tanukiex.BuildTrigger.remove(credentials, 12345, token)
    ```
  """
  @spec remove(Credentials.t, integer, String.t) :: Tanukiex.Response
  def remove(credentials, project_id, token) do
    delete(credentials, "/projects/#{project_id}/triggers/#{token}")
  end
end