defmodule Tanukiex.Branch do
  import Tanukiex

  @moduledoc """
  This module maps all branch api resources to a matching function.

  *** API Reference: *** http://docs.gitlab.com/ce/api/branches.html  
  """

  @doc """
  Get a list of repository branches from a project, sorted by name alphabetically.  

  *** API Reference: *** 
  http://docs.gitlab.com/ce/api/branches.html#list-repository-branches  
  
    ```
    Tanukiex.Branch.list_by_project(credentials, 12345)
    ```
  """
  @spec list_by_project(Credentials.t, integer) :: Tanukiex.Response
  def list_by_project(credentials, project_id) do
    get(credentials, "/projects/#{project_id}/repository/branches")
  end

  @doc """
  Get a single repository branch from a project.  

  *** API Reference: *** 
  http://docs.gitlab.com/ce/api/branches.html#get-single-repository-branch
  
    ```
    Tanukiex.Branch.get_single_by_project(credentials, 12345, 678910)
    ```
  """
  @spec get_single_by_project(Credentials.t, integer, String.t) :: Tanukiex.Response
  def get_single_by_project(credentials, project_id, branch) do
    get(credentials, "/projects/#{project_id}/repository/branches/#{branch}")
  end

  @doc """
  Protects a single project repository branch. This is an idempotent function,
   protecting an already protected repository branch still returns a 200 OK status code.  

  *** API Reference: *** 
  http://docs.gitlab.com/ce/api/branches.html#protect-repository-branch
  
    ```
    Tanukiex.Branch.protect(credentials, 12345, 678910)
    Tanukiex.Branch.protect(credentials, 12345, 678910, true, true)
    ```

  *** Arguments: ***  

    * branch - branch name
    * developers_can_push (optional) - boolean to prevent/allow developers to push to the branch 
    * developers_can_merge (optional) - boolean to prevent/allow developers to merge the branch

  """
  @spec protect(Credentials.t, integer, String.t, boolean, boolean) :: Tanukiex.Response
  def protect(credentials, project_id, branch, developers_can_push \\ false, developers_can_merge \\ false) do
    put(credentials, "/projects/#{project_id}/repository/branches/#{branch}/protect", %{developers_can_push: developers_can_push, 
                                                                                        developers_can_merge: developers_can_merge})
  end

  @doc """
  Unprotects a single project repository branch. This is an idempotent function, 
  unprotecting an already unprotected repository branch still returns a 200 OK status code.  

  *** API Reference: *** 
  http://docs.gitlab.com/ce/api/branches.html#unprotect-repository-branch
  
    ```
    Tanukiex.Branch.unprotect(credentials, 12345, "master")
    ```

  """
  @spec unprotect(Credentials.t, integer, String.t) :: Tanukiex.Response
  def unprotect(credentials, project_id, branch) do
    put(credentials, "/projects/#{project_id}/repository/branches/#{branch}/unprotect")
  end

  @doc """
  Create a repository branch  

  *** API Reference: *** 
  http://docs.gitlab.com/ce/api/branches.html#create-repository-branch
  
    ```
    Tanukiex.Branch.create(credentials, 12345, "new-branch-name", "master")
    ```

  *** Arguments: ***  

    * branch_name: name of the new branch
    * ref: The branch name or commit SHA to create branch from

  """
  @spec create(Credentials.t, integer, String.t, String.t) :: Tanukiex.Response
  def create(credentials, project_id, branch_name, ref) do
    post(credentials, "/projects/#{project_id}/repository/branches", %{branch_name: branch_name, ref: ref})
  end

  @doc """
  Deletes a repository branch  

  *** API Reference: *** 
  http://docs.gitlab.com/ce/api/branches.html#delete-repository-branch
  
    ```
    Tanukiex.Branch.delete(credentials, 12345, "a-branch-name")
    ```

  """
  @spec delete(Credentials.t, integer, String.t) :: Tanukiex.Response
  def delete(credentials, project_id, branch) do
    delete(credentials, "/projects/#{project_id}/repository/branches/#{branch}")
  end
end
