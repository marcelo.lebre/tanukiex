defmodule Tanukiex.BuildVariables do
  import Tanukiex

  @moduledoc """
  This module maps all build variables api resources to a matching function.

  *** API Reference: *** https://docs.gitlab.com/ce/api/build_variables.html#build-variables
  """

  @doc """
  Get list of a project's build variables.

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/build_variables.html#list-project-variables
  
    ```
    Tanukiex.BuildVariables.list_by_project(credentials, 12345)
    ```
  """
  @spec list_by_project(Credentials.t, integer) :: Tanukiex.Response
  def list_by_project(credentials, project_id) do
    get(credentials, "/projects/#{project_id}/variables")
  end

  @doc """
  Get the details of a project's specific build variable.

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/build_variables.html#show-variable-details
  
    ```
    Tanukiex.BuildVariables.details(credentials, 12345, key)
    ```
  """
  @spec details(Credentials.t, integer, String.t) :: Tanukiex.Response
  def details(credentials, project_id, key) do
    get(credentials, "/projects/#{project_id}/variables/#{key}")
  end

  @doc """
  Create a new build variable.

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/build_variables.html#create-variable
  
    ```
    Tanukiex.BuildVariables.create(credentials, 12345, key, value)
    ```
  """
  @spec create(Credentials.t, integer, String.t, String.t) :: Tanukiex.Response
  def create(credentials, project_id, key, value) do
    post(credentials, "/projects/#{project_id}/variables", %{key: key, value: value})
  end

  @doc """
  Update a project's build variable.

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/build_variables.html#update-variable
  
    ```
    Tanukiex.BuildVariables.update(credentials, 12345, key, value)
    ```
  """
  @spec update(Credentials.t, integer, String.t, String.t) :: Tanukiex.Response
  def update(credentials, project_id, key, value) do
    put(credentials, "/projects/#{project_id}/variables/#{key}", %{key: key, value: value})
  end

  @doc """
  Remove a project's build variable.

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/build_variables.html#remove-variable
  
    ```
    Tanukiex.BuildVariables.remove(credentials, 12345, key, value)
    ```
  """
  @spec remove(Credentials.t, integer, String.t, String.t) :: Tanukiex.Response
  def remove(credentials, project_id, key, value) do
    delete(credentials, "/projects/#{project_id}/variables/#{key}", %{key: key, value: value})
  end
end