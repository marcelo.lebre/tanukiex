defmodule Tanukiex.AwardEmoji do
  import Tanukiex

  @moduledoc """
  This module maps all award emoji api resources to a matching function.

  *** API Reference: *** http://docs.gitlab.com/ce/api/award_emoji.html  
  """

  @doc """
  Gets a list of all award emoji from an issue.  

  *** API Reference: *** 
  http://docs.gitlab.com/ce/api/award_emoji.html#list-an-awardables-award-emoji  
  
    ```
    Tanukiex.AwardEmoji.list_by_issue(credentials, 16180, 3141592)
    ```
  """
  @spec list_by_issue(Credentials.t, integer, integer) :: Tanukiex.Response
  def list_by_issue(credentials, project_id, issue_id) do
    get(credentials, "/projects/#{project_id}/issues/#{issue_id}/award_emoji")
  end

  @doc """
  Gets a list of all award emoji from a merge request  

  *** API Reference: 
  *** http://docs.gitlab.com/ce/api/award_emoji.html#list-an-awardables-award-emoji  
  
    ```
    Tanukiex.AwardEmoji.list_by_merge_request(credentials, 112233, 445566)
    ```
  """
  @spec list_by_merge_request(Credentials.t, integer, integer) :: Tanukiex.Response
  def list_by_merge_request(credentials, project_id, merge_request_id) do
    get(credentials, "/projects/#{project_id}/merge_requests/#{merge_request_id}/award_emoji")
  end

  @doc """
  Gets a single award id from an issue

  *** API Reference: 
  *** http://docs.gitlab.com/ce/api/award_emoji.html#get-single-award-emoji  
  
    ```
    Tanukiex.AwardEmoji.get_single_from_issue(credentials, 1111, 2222, 3333)
    ```
  """
  @spec get_single_from_issue(Credentials.t, integer, integer, integer) :: Tanukiex.Response
  def get_single_from_issue(credentials, project_id, issue_id, award_id) do
    get(credentials, "/projects/#{project_id}/issues/#{issue_id}/award_emoji/#{award_id}")
  end

  @doc """
  Gets a single award id from a merge request  

  *** API Reference: 
  *** http://docs.gitlab.com/ce/api/award_emoji.html#get-single-award-emoji  
  
    ```
    Tanukiex.AwardEmoji.get_single_from_merge_request(credentials, 1111, 2222, 3333)
    ```
  """
  @spec get_single_from_merge_request(Credentials.t, integer, integer, integer) :: Tanukiex.Response
  def get_single_from_merge_request(credentials, project_id, merge_request_id, award_id) do
    get(credentials, "/projects/#{project_id}/merge_requests/#{merge_request_id}/award_emoji/#{award_id}")
  end

  @doc """
  Attaches an award emoji to an issue 

  *** API Reference: 
  *** http://docs.gitlab.com/ce/api/award_emoji.html#award-a-new-emoji  
  
    ```
    Tanukiex.AwardEmoji.award_emoji_to_issue(credentials, 111, 222, "blowfish")
    ```
  """
  @spec award_emoji_to_issue(Credentials.t, integer, integer, String.t) :: Tanukiex.Response
  def award_emoji_to_issue(credentials, project_id, issue_id, name) do
    post(credentials, "/projects/#{project_id}/issues/#{issue_id}/award_emoji", "name=" <> name)
  end

  @doc """
  Attaches an award emoji to a merge request 

  *** API Reference: 
  *** http://docs.gitlab.com/ce/api/award_emoji.html#award-a-new-emoji  
  
    ```
    Tanukiex.AwardEmoji.award_emoji_to_issue(credentials, 111, 222, "blowfish")
    ```
  """
  @spec award_emoji_to_merge_request(Credentials.t, integer, integer, String.t) :: Tanukiex.Response
  def award_emoji_to_merge_request(credentials, project_id, merge_request_id, name) do
    post(credentials, "/projects/#{project_id}/merge_requests/#{merge_request_id}/award_emoji", "name=" <> name)
  end

  @doc """
  Delete an awarded emoji of an issue 

  *** API Reference: 
  *** http://docs.gitlab.com/ce/api/award_emoji.html#delete-an-award-emoji    
  
    ```
    Tanukiex.AwardEmoji.delete_award_emoji_from_issue(credentials, 111, 222, 333)
    ```
  """
  @spec delete_award_emoji_from_issue(Credentials.t, integer, integer, integer) :: Tanukiex.Response
  def delete_award_emoji_from_issue(credentials, project_id, issue_id, award_id) do
    delete(credentials, "/projects/#{project_id}/issues/#{issue_id}/award_emoji/#{award_id}")
  end

  @doc """
  Delete an awarded emoji of a merge request 

  *** API Reference: 
  *** http://docs.gitlab.com/ce/api/award_emoji.html#delete-an-award-emoji  
  
    ```
    Tanukiex.AwardEmoji.delete_award_emoji_from_merge_request(credentials, 111, 222, 333, 444)
    ```
  """
  @spec delete_award_emoji_from_merge_request(Credentials.t, integer, integer, integer) :: Tanukiex.Response
  def delete_award_emoji_from_merge_request(credentials, project_id, merge_request_id, award_id) do
    delete(credentials, "/projects/#{project_id}/merge_requests/#{merge_request_id}/award_emoji/#{award_id}")
  end

  @doc """
  List a note's award emoji 

  *** API Reference: 
  *** http://docs.gitlab.com/ce/api/award_emoji.html#list-a-notes-award-emoji  
  
    ```
    Tanukiex.AwardEmoji.list_from_note(credentials, 1111, 2222, 3333)
    ```
  """
  @spec list_from_note(Credentials.t, integer, integer, integer) :: Tanukiex.Response
  def list_from_note(credentials, project_id, issue_id, note_id) do
    get(credentials, "/projects/#{project_id}/issues/#{issue_id}/notes/#{note_id}/award_emoji")
  end

  @doc """
  Get a single note's award emoji  

  *** API Reference: 
  *** http://docs.gitlab.com/ce/api/award_emoji.html#get-single-notes-award-emoji 
  
    ```
    Tanukiex.AwardEmoji.get_single_from_note(credentials, 1111, 2222, 3333, 4444)
    ```
  """
  @spec get_single_from_note(Credentials.t, integer, integer, integer, integer) :: Tanukiex.Response
  def get_single_from_note(credentials, project_id, issue_id, note_id, award_id) do
    get(credentials, "/projects/#{project_id}/issues/#{issue_id}/notes/#{note_id}/award_emoji/#{award_id}")
  end

  @doc """
  Get a single note's award emoji  

  *** API Reference: 
  *** http://docs.gitlab.com/ce/api/award_emoji.html#award-a-new-emoji-on-a-note 
  
    ```
    Tanukiex.AwardEmoji.award_emoji_to_note(credentials, 1111, 2222, 3333, "footbal")
    ```
  """
  @spec award_emoji_to_note(Credentials.t, integer, integer, integer, String.t) :: Tanukiex.Response
  def award_emoji_to_note(credentials, project_id, issue_id, note_id, name) do
    post(credentials, "/projects/#{project_id}/issues/#{issue_id}/notes/#{note_id}/award_emoji", "name=" <> name)
  end

  @doc """
  Delete an awarded emoji from note 

  *** API Reference: 
  *** http://docs.gitlab.com/ce/api/award_emoji.html#delete-an-award-emoji-1    
  
    ```
    Tanukiex.AwardEmoji.delete_award_emoji_from_note(credentials, 111, 222, 333, 4444)
    ```
  """
  @spec delete_award_emoji_from_note(Credentials.t, integer, integer, integer, integer) :: Tanukiex.Response
  def delete_award_emoji_from_note(credentials, project_id, issue_id, note_id, award_id) do
    delete(credentials, "/projects/#{project_id}/issues/#{issue_id}/notes/#{note_id}/award_emoji/#{award_id}")
  end
end
