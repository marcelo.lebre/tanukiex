defmodule Tanukiex.Commits do
  import Tanukiex

  @moduledoc """
  This module maps all commits api resources to a matching function.

  *** API Reference: *** https://docs.gitlab.com/ce/api/commits.html  
  """

  @doc """
  ### Description
  Get a list of repository commits in a project.
  

  ### API Reference
  https://docs.gitlab.com/ce/api/commits.html#list-repository-commits
    

  ### Example
    ```
    Tanukiex.Commits.list_by_project(credentials, 12345)
    ```
  """
  @spec list_by_project(Credentials.t, integer) :: Tanukiex.Response
  def list_by_project(credentials, project_id) do
    get(credentials, "/projects/#{project_id}/repository/commits")
  end

  @doc """
  ### Description
  Create a commit with multiple files and actions

  ### API Reference
  https://docs.gitlab.com/ce/api/commits.html#create-a-commit-with-multiple-files-and-actions
  
    ```
    action = CommitAction.create("class1.rb", "class2.rb", "test", "text")
      |> CommitActions.build
    
    commit = %Commits.Attributes{ 
                      branch_name: "master",
                      commit_message: "commit message test",
                      actions: actions,
                      author_email: "marcelo.lebre@gmail.com",
                      author_name: "Marcelo Lebre"
                    }

    Tanukiex.Commits.create(credentials, 12345, commit)
    ```
  """
  @spec create(Credentials.t, integer, Attributes.t) :: Tanukiex.Response
  def create(credentials, project_id, commit) do
    post(credentials, "/projects/#{project_id}/repository/commits", commit)
  end

  @doc """
  ### Description
  Get a specific commit identified by the commit hash or name of a branch or tag.

  ### API Reference
  https://docs.gitlab.com/ce/api/commits.html#get-a-single-commit

  ### Example
    ```
    Tanukiex.Commits.get_single_commit(credentials, 12345, "master")
    Tanukiex.Commits.get_single_commit(credentials, 12345, "f48143247f025f14dd489f0b68f615ccc0ce7eac")
    ```
  """
  @spec get_single_commit(Credentials.t, integer, String.t) :: Tanukiex.Response
  def get_single_commit(credentials, project_id, sha) do
    get(credentials, "/projects/#{project_id}/repository/commits/#{sha}")
  end

  @doc """
  ### Description
  Get the diff of a commit in a project.

  ### API Reference
  https://docs.gitlab.com/ce/api/commits.html#get-the-diff-of-a-commit

  ### Example
    ```
    Tanukiex.Commits.diff(credentials, 12345, "master")
    Tanukiex.Commits.diff(credentials, 12345, "f48143247f025f14dd489f0b68f615ccc0ce7eac")
    ```
  """
  @spec diff(Credentials.t, integer, String.t) :: Tanukiex.Response
  def diff(credentials, project_id, sha) do
    get(credentials, "/projects/#{project_id}/repository/commits/#{sha}/diff")
  end

  @doc """
  ### Description
  Get the comments of a commit in a project.

  ### API Reference
  https://docs.gitlab.com/ce/api/commits.html#get-the-comments-of-a-commit

  ### Example
    ```
    Tanukiex.Commits.comments(credentials, 12345, "master")
    Tanukiex.Commits.comments(credentials, 12345, "f48143247f025f14dd489f0b68f615ccc0ce7eac")
    ```
  """
  @spec comments(Credentials.t, integer, String.t) :: Tanukiex.Response
  def comments(credentials, project_id, sha) do
    get(credentials, "/projects/#{project_id}/repository/commits/#{sha}/comments")
  end

    @doc """
  ### Description
  Get the statuses of a commit in a project.

  ### API Reference
  https://docs.gitlab.com/ce/api/commits.html#get-the-status-of-a-commit

  ### Example
    ```
    Tanukiex.Commits.statuses(credentials, 12345, "master")
    Tanukiex.Commits.statuses(credentials, 12345, "f48143247f025f14dd489f0b68f615ccc0ce7eac")
    ```
  """
  @spec statuses(Credentials.t, integer, String.t) :: Tanukiex.Response
  def statuses(credentials, project_id, sha) do
    get(credentials, "/projects/#{project_id}/repository/commits/#{sha}/statuses")
  end

  @doc """
  ### Description
  Adds a comment to a commit.

  ### API Reference
  https://docs.gitlab.com/ce/api/commits.html#post-comment-to-commit
  
  ### Example
    ```
    comment = %Tanukiex.Commits.Comment{
                                      note: "Nice picture man\!",
                                      path: "dudeism.md",
                                      line: 2,
                                      line_type: "new"
                                    }

    Tanukiex.Commits.add_comment(credentials, 12345, comment)
    ```
  """
  @spec add_comment(Credentials.t, integer, String.t, Comment.t) :: Tanukiex.Response
  def add_comment(credentials, project_id, sha, comment) do
    post(credentials, "/projects/#{project_id}/repository/commits/#{sha}/comments", comment)
  end

  @doc """
  ### Description
  Adds or updates a build status of a commit.

  ### API Reference
  https://docs.gitlab.com/ce/api/commits.html#post-the-build-status-to-a-commit
  
  ### Example
    ```
    build_status = %Tanukiex.Commits.BuildStatus{
                                                state: "success",
                                                ref: "master",
                                                name: "default",
                                                target_url: "https://docs.gitlab.com/ce/api/commits.html#post-the-build-status-to-a-commit",
                                                description: "Short description"
                                              }

    Tanukiex.Commits.add_build_status(credentials, 12345, build_status)
    ```
  """
  @spec add_build_status(Credentials.t, integer, String.t, BuildStatus.t) :: Tanukiex.Response
  def add_build_status(credentials, project_id, sha, build_status) do
    post(credentials, "/projects/#{project_id}/statuses/#{sha}", build_status)
  end
end
