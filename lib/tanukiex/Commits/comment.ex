defmodule Tanukiex.Commits.Comment do
  @moduledoc """
    Tanukiex.Commits.Comment module is responsible for encapsulating
    a commit comment.
  """

  @typedoc """
  Commit comment struct is used to build the comment map.  
  """
  @type t :: %__MODULE__{
                          note: String.t,
                          path: String.t,
                          line: integer,
                          line_type: String.t
                        }

  @doc """
  Build a commit comment struct.
    * `note` - The text of the comment
    * `path` - The file path relative to the repository
    * `line` - The line number where the comment should be placed
    * `line_type` - The line type. Takes new or old as arguments

    ```
    %Tanukiex.Commits.Comment{
                               note: "Nice picture man\!",
                               path: "dudeism.md",
                               line: 2,
                               line_type: "new"
                             }
    ```
  """

  defstruct note: nil,
            path: nil,
            line: 0,
            line_type: nil
end
