defmodule Tanukiex.Commits.Actions do
  alias Tanukiex.Commits.Action

  @moduledoc """
  Tanukiex.Commit.Actions module is responsible for encapsulating
  an enumerate of actions.
  """

  @typedoc """
  Actions struct is used to build the Actions map.  
  """
  @type commit_actions :: [Action.t]

  @doc """
  Build a commit actions struct.
    * `actions` - enumerate of %Tanukiex.Commits.Action{} structs

    ```
    %Tanukiex.Commits.Actions{actions: actions}
    ```
  """

  @spec build(Action.t) :: commit_actions
  def build(action), do: [action]

  @spec add(commit_actions, Action.t) :: commit_actions
  def add(actions, action), do: [actions | action]

  @spec default :: commit_actions
  def default, do: []
end
