defmodule Tanukiex.Commits.BuildStatus do
  @moduledoc """
  Tanukiex.Commit.BuildStatus module is responsible for encapsulating
  a commit's build status.
  """
  @typedoc """
  BuildStatus struct is used to build the build status map.  
  """
  @type t :: %__MODULE__{
                          state: String.t,
                          ref: String.t,
                          name: String.t,
                          target_url: String.t,
                          description: String.t
                        }
  @doc """
  Build a commit comment struct.
    * `state` - The state of the status. Can be one of the following: pending, running, success, failed, canceled
    * `ref` - The ref (branch or tag) to which the status refers
    * `name or context` - The label to differentiate this status from the status of other systems. Default value is default
    * `target_url` - The target URL to associate with this status
    * `description` - The short description of the status
    ```
    %Tanukiex.Commits.BuildStatus{
                                 state: "success",
                                 ref: "master",
                                 name: "default",
                                 target_url: "https://docs.gitlab.com/ce/api/commits.html#post-the-build-status-to-a-commit",
                                 description: "Short description"
                               }
    ```
  """
  defstruct state: nil,
            ref: nil,
            name: "default",
            target_url: nil,
            description: nil
end
