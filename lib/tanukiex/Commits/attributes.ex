defmodule Tanukiex.Commits.Attributes do
  alias Tanukiex.Commits.Actions

  @moduledoc """
    Tanukiex.Commits.Attributes module is responsible for encapsulating
    a commit info and actions.
  """

  @typedoc """
  Attributes struct is used to build the commit map with data and actions.  
  """
  @type t :: %__MODULE__{
                          branch_name: String.t,
                          commit_message: String.t,
                          actions: CommitActions.t,
                          author_email: String.t,
                          author_name: String.t
                        }

  @doc """
  Build a commit actions struct.
    * `branch_name` - name of the repo branch, i.e.: "master"
    * `commit_message` - commit message, i.e.: "initial commit"
    * `author_name` - Specify the commit author's name
    * `author_email` - Specify the commit author's email address
    * `actions` - An array of action hashes to commit as a batch.

    ```
    %Tanukiex.Commits.Attributes{
                                  branch_name: "master",
                                  commit_message: "initial commit",
                                  actions: actions,
                                  author_email: "marcelo.lebre@gmail.com",
                                  author_name: "Marcelo Lebre"
                                }
    ```
  """

  defstruct branch_name: nil,
            commit_message: nil,
            author_email: nil,
            author_name: nil,
            actions: Actions.default
end
